﻿namespace Synoptic.Service
{
    public interface IDaemon
    {
        void Start();
        void Stop();
    }
}